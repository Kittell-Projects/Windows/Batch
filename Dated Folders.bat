@echo off

set cyear=%date:~-4,4%
set cday=%date:~-7,2%
set cmonth=%date:~-10,2%
SET cdate=%cmonth%%cday%%cyear%

if not exist "C:\qcsafetynet\ClaspInstall\" (md "C:\qcsafetynet\ClaspInstall\")

cd "C:\qcsafetynet\ClaspInstall\"
	
for /f %%J in ('dir/b/od %cdate%-*') do (
	set nLatestSafetyNet=%%~nJ )
rem	echo %nLatestSafetyNet%
	set nNewSafetyNet=%nLatestSafetyNet:~9,3%
rem	echo %nNewSafetyNet%
	set /a nNewSafetyNet+=1
rem	echo %nNewSafetyNet%
	md "C:\qcsafetynet\ClaspInstall\%cdate%-%nNewSafetyNet%"
)