@rem off
@echo off
color 1f

echo Setting DHCP...

netsh interface ip set address "Local Area Connection" dhcp
netsh interface ip set address "Wireless Network Connection" dhcp

netsh interface ip set dns name="Local Area Connection" source=static addr=208.67.222.222
netsh interface ip add dns name="Local Area Connection" addr=208.67.220.220

netsh interface ip set dns name="Wireless Network Connection" source=static addr=208.67.222.222
netsh interface ip add dns name="Wireless Network Connection" addr=208.67.220.220

ipconfig /release
ipconfig /renew
ipconfig /flushdns

pause