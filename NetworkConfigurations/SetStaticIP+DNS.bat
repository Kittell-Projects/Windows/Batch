@echo off
netsh interface ip set address name="Local Area Connection" source=static addr=10.0.0.54 mask=255.255.255.0
netsh interface ip set address name="Local Area Connection" gateway=10.0.0.1 gwmetric=0
netsh interface ip set dns name="Local Area Connection" source=static addr=208.67.222.222
netsh interface ip add dns name = "Local Area Connection" addr =208.67.220.220
ipconfig /all
pause