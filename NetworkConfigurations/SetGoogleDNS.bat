@echo off
netsh interface ip set dns name="Local Area Connection" source=static addr=8.8.8.8
netsh interface ip add dns name="Local Area Connection" addr=8.8.4.4
netsh interface ip set dns name="Wireless Network Connection" source=static addr=8.8.8.8
netsh interface ip add dns name="Wireless Network Connection" addr=8.8.4.4
ipconfig /release
ipconfig /renew
ipconfig /flushdns