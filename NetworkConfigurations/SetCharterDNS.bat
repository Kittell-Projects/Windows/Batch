@rem off
@echo off
color 1f

echo Setting DNS...

netsh interface ip set dns name="Local Area Connection" source=static addr=24.247.24.53
netsh interface ip add dns name="Local Area Connection" addr=24.178.162.3

netsh interface ip set dns name="Wireless Network Connection" source=static addr=24.247.24.53
netsh interface ip add dns name="Wireless Network Connection" addr=24.178.162.3


ipconfig /release
ipconfig /renew
ipconfig /flushdns
pause