@rem off
@echo off
color 1f

echo Setting DHCP...

netsh interface ip set address "Local Area Connection" dhcp
netsh interface ip set dns name="Local Area Connection" source=dhcp
netsh interface ip set address "Wireless Network Connection" dhcp
netsh interface ip set dns name="Wireless Network Connection" source=dhcp

netsh interface ip set address "Ethernet 4" dhcp
netsh interface ip set dns name="Ethernet 4" source=dhcp
netsh interface ip set address "Wi-Fi" dhcp
netsh interface ip set dns name="Wi-Fi" source=dhcp

ipconfig /release
ipconfig /renew
ipconfig /flushdns