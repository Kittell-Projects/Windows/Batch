@echo off
@rem off

attrib -r -a -s -h .DS_Store /S /D
attrib -r -a -s -h desktop.ini /S /D
attrib -r -a -s -h SyncToy_*.dat /S /D
attrib -r -a -s -h thumbs.db /S /D
del /f /s /q .DS_Store
del /f /s /q desktop.ini
del /f /s /q SyncToy_*.dat
del /f /s /q THUMBS.DB
del /q /s "IconCache.db" > IconCache.txt
reg delete "HKCR\Local Settings\Software\Microsoft\Windows\Shell\MuiCache" /va /f
reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\OpenSaveMRU" /va /f
reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\OpenSaveMRU\reg" /va /f
del /f /q "%APPDATA%\Microsoft\Outlook\Outlook.NK2"

cd\
cd windows\prefetch
del *.* /q
cd\
rd /q /s C:\I386\
rd /q /s "C:\Windows\Driver Cache\I386\"
rd /q /s "C:\Windows\ServicePackFiles\I386\"
cd\
del /s /q "C:\Windows\Downloaded Installations\*.*"
del /s /q "C:\Windows\SoftwareDistribution\Download\*.*"