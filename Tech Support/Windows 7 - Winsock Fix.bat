@echo off
@rem off
color 4f

goto menu
:menu
cls
echo WARNING - This will attempt to fix Winsock errors
echo.
echo Choose:
echo 1 Cancel And Close
echo 2 Run Fix
echo.
:choice
set /P C=#
if "%C%"=="1" goto close
if "%C%"=="2" goto RunFix
goto choice


:close
exit
goto menu

:RunFix
echo.
echo Running Fix....
echo.
echo Resetting winsock entries
netsh winsock reset catalog
echo Resetting TCP/IP stack entries
netsh int ip reset reset.log hit
cls
echo Please restart your computer.
pause
exit
goto menu