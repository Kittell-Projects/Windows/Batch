@echo off
@rem off

color 1f

cls
echo Applying Services Fix

sc config "Alerter" start= demand
sc config "ALG" start= demand
sc config "appmgt" start= demand
sc config "wuaUserv" start= auto
sc config "BITS" start= demand
sc config "Clipsrv" start= demand
sc config "Event System" start= demand
sc config "COMSysApp" start= demand
sc config "Browser" start= auto
sc config "CryptSvc" start= auto
sc config "DcomLaunch" start= auto
sc config "Dhcp" start= auto
sc config "TrkWks" start= auto
sc config "msdtc" start= demand
sc config "Dnscache" start= auto
sc config "Ersvc" start= auto
sc config "EventLog" start= auto
sc config "FastUserSwitching Compatibility " start= demand
sc config "helpsvc" start= auto
sc config "HTTPFilter" start= demand
sc config "HidServ" start= Disabled
sc config "ImapiService" start= demand
sc config "cisvc" start= demand
sc config "PolicyAgent" start= auto
sc config "Dmserver" start= auto
sc config "Dmadmin" start= demand
sc config "Messenger" start= demand
sc config "swprv" start= demand
sc config "Netlogon" start= auto
sc config "Nmnsrvc" start= demand
sc config "Netman" start= demand
sc config "NetDDE" start= demand
sc config "NetDDEdsdm" start= demand
sc config "xmlprov" start= demand
sc config "nla" start= demand
sc config "NtLmSsp" start= demand
sc config "sysmonLog" start= demand
sc config "PlugPlay" start= auto
sc config "WmdmPmSN" start= auto
sc config "Spooler" start= auto
sc config "ProtectedStorage" start= auto
sc config "rsvp" start= demand
sc config "Rasauto" start= demand
sc config "Rasman" start= demand
sc config "RDSessMgr" start= demand
sc config "RpcLocator" start= demand
sc config "RpcSs" start= auto
sc config "RemoteRegistry" start= auto
sc config "Ntmssvc" start= demand
sc config "RemoteAccess" start= demand
sc config "secLogon" start= auto
sc config "SamSs" start= auto
sc config "wscsvc" start= auto
sc config "LanmanServer" start= auto
sc config "ShellHWDetection" start= auto
sc config "ScardSrv" start= demand
sc config "ScardDrv" start= demand
sc config "Snmp" start= demand
sc config "SSDPSRV" start= demand
sc config "SENS" start= auto
sc config "srservice" start= auto
sc config "Schedule" start= auto
sc config "lmHosts" start= auto
sc config "TapiSrv" start= demand
sc config "TlntSvr" start= demand
sc config "TermService" start= demand
sc config "Themes" start= auto
sc config "UPNPhost" start= demand
sc config "uploadmgr" start= auto
sc config "UPS" start= demand
sc config "VSS" start= demand
sc config "WebClient" start= auto
sc config "AudioSrv" start= auto
sc config "SharedAccess" start= auto
sc config "stisvc" start= demand
sc config "MSIServer" start= demand
sc config "WinMgmt" start= auto
sc config "Wmi" start= demand
sc config "W32time" start= auto
sc config "WZCSVC" start= auto
sc config "WmiApSrv" start= demand
sc config "lanmanworkstation" start= auto

cls
echo Services Fix has finished
pause