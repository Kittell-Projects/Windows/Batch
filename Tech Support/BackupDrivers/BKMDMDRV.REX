/* Backup modem driver files with DEVCON */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* Sample DEVCON output:                                                               */
/*                                                                                     */
/* PCI\VEN_127A&DEV_1035&SUBSYS_1035127A&REV_08\4&11CD5334&0&48F0                      */
/*     Name: CXT1035 - HCF PCI Modem                                                   */
/*     Driver installed from c:\winnt\inf\oem8.inf [Modem]. 2 file(s) used by driver:  */
/*         C:\WINNT\system32\DRIVERS\winachcf.sys                                      */
/*         C:\WINNT\system32\cxt1035.cty                                               */
/* 1 matching device(s) found.                                                         */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

"@ECHO OFF"
Trace Off

/* Check OS type (should be 32 bits Windows)*/
Parse Upper Source os .
If os <> "WIN32" Then Call Syntax

/* Check command line arguments */
Parse Arg test
If test <> "" Then Call Syntax

/* Initialize RexxUtil library */
If RxFuncQuery( "sysloadfuncs" ) <> 0 Then Do
	initRc = RxFuncAdd( "sysloadfuncs", "RexxUtil", "sysloadfuncs" )
	If initRc = 0 Then Do
		Call sysloadfuncs
	End
	Else Do
		Say
		Say "Error "||initRc||" while trying to initialize RegUtil (RexxUtil)"
		Say
		Say "This script requires Patrick TJ McPhee's RegUtil library,"
		Say "available at http://www.interlog.com/~ptjm/"
		Say
		Say "Do yo want to download it now? [y/N]"
		Say
		download = Translate( SysGetKey( "NOECHO" ) )
		If download = "Y" Then Do
			Address SYSTEM "http://www.interlog.com/~ptjm/"
			Say "Install the downloaded library file and try again."
		End
		Else Do
			Say "Download and install the library and try again."
		End
		Exit 1
	End
End

/* Check DEVCON's availability */
Address SYSTEM "DEVCON.EXE /? >NUL 2>&1"
If rc <> 0 Then Do
	Say
	Say "This script requires Microsoft's DEVCON.EXE, available at"
	Say "http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q311272"
	Say
	Say "Do yo want to download it now? [y/N]"
	Say
	download = Translate( SysGetKey( "NOECHO" ) )
	If download = "Y" Then Do
		Address SYSTEM "http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q311272"
		Say "Install the downloaded file and try again."
	End
	Else Do
		Say "Download the tool and try again."
	End
	Exit 1
End

/* Use DEVCON to retrieve info, and store it in an array */
Address SYSTEM 'DEVCON.EXE DriverFiles =Modem 2>NUL | RXQUEUE'
empty = 0
Do i = 1 By 1 Until Queued( ) = 0
	Parse Pull line.i
	line.i = Strip( line.i, "B", " " )
	If line.i = "" Then Do
		empty = empty + 1
		if empty > 5 Then Leave
	End
	line.0 = i
End

/* Interpret DEVCON's output line by line */
nextLine  = "ID"
fileCount = 0
driver.   = ""
driver.0  = 0
j = 0
Do i = 1 By 1 To line.0
	Select
		When Pos( "matching device(s) found.", line.i ) > 0 Then Leave i       /* Last line of output   */
		When nextLine = "ID" Then Do                                           /* First line for driver */
			Parse Value line.i With root"\"str1"&"str2"\"str3
			If str3 <> "" Then Do
				j = j + 1
				driver.0 = j
				driver.j.0 = 0
				nextLine = "Name"
			End
		End
		When nextLine = "Name" Then Do                                         /* Line with device name */
			Parse Value line.i With "Name: "name
			If name <> "" Then Do
				name = EscName( name )
				driver.j = name
				k = 0
				nextLine = "inf"
			End
		End
		When nextLine = "inf" Then Do                                          /* Line with inf file name */
			Parse Value line.i With "Driver installed from "inf" ["key"]. "numFiles" file(s) used by driver:"
			If numFiles <> "" Then Do
				k = k + 1
				driver.j.k = inf
				driver.j.0 = k
				nextLine = "files"
			End
		End
		When nextLine = "files" Then Do                                        /* Actual driver files */
			Parse Value line.i With file
			If file <> "" Then Do
				k = k + 1
				If k = numFiles + 1 Then Do
					nextLine = "ID"
				End
				driver.j.k = file
				driver.j.0 = k
			End
		End
		Otherwise Nop
	End
End

/* Create a target "root" directory */
If SysMkDir( "Modem" ) <> 0 Then Do
	Call SysFileTree "Modem", "exist.", "D"
	If exist.0 <> 1 Then Do
		Say "Error creating directory Modem"
		Exit 1
	End
End
Do i = 1 By 1 To driver.0
	Say "Backing up drivers for modem "||driver.i
	/* Create a subdirectory with the device name */
	If SysMkDir( "Modem\"||driver.i ) <> 0 Then Do
		Call SysFileTree "Modem\"||driver.i, "exist.", "D"
		If exist.0 <> 1 Then Do
			Say "Error creating directory Modem\"||driver.i
			Exit 1
		End
	End
	Do j = 1 To driver.i.0
		driverFile = driver.i.j
		Do Until driverFile = ""
			Parse Value driverFile With dummy"\"driverFile
		End
		driverFile = dummy
		copyRc = SysCopyObject( driver.i.j, "Modem\"||driver.i||"\"||driverFile )
		If copyRc <> 0 Then Do
			Say "Error "||copyRc||" while copying "||driver.i.j
			Exit 1
		End
	End
End

Say "Your modem drivers are now stored in the directory Modem"

/* Done */
Exit 0


EscName: procedure
	name = Arg( 1 )
	name = Translate( name, "",  "|" )
	name = Translate( name, "",  "?" )
	name = Translate( name, "",  ":" )
	name = Translate( name, "",  ";" )
	name = Translate( name, "", "," )
	name = Translate( name, "",  "/" )
	name = Translate( name, "",  "\" )
	name = Translate( name, "[", "(" )
	name = Translate( name, "]", ")" )
	name = Translate( name, "and", "&" )
Return name


Syntax:
	Say
	Say "BkMdmDrv,  Version 1.00 for Windows 2000 / XP"
	Say "Backup driver files for every modem installed"
	Say
	Say "Usage:  BKMDMDRV.REX"
	Say
	Say "Note:   This script requires Microsoft's DEVCON.EXE, available at"
	Say "        http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q311272"
	Say "        This script also requires Patrick TJ McPhee's RegUtil,"
	Say "        available at http://www.interlog.com/~ptjm/"
	Say "        You will be prompted for download if either isn't found."
	Say
	Say "Written by Rob van der Woude"
	Say "http://www.robvanderwoude.com"
	Exit 1
Return
