@echo off
set cdrom=none
for %%a in (c d e f g h i j k l m n o p q r s t u v w x y z) do (
fsutil.exe fsinfo drivetype %%a:|find "CD-ROM">nul&&set cdrom=%%a:
)
echo CD-ROM drive letter: %cdrom%
pause