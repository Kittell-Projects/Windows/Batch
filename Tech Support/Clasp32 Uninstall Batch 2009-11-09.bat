REM  QBFC Project Options Begin
REM  HasVersionInfo: Yes
REM  Companyname: Quantum Computers LLC
REM  Productname: QC-ClaspUninstaller
REM  Filedescription: QC-ClaspUninstaller
REM  Copyrights: 2009 Quantum Computers LLC
REM  Trademarks: 
REM  Originalname: 
REM  Comments: 
REM  Productversion:  2. 0. 0. 0
REM  Fileversion:  2. 0. 0. 0
REM  Internalname: QC-ClaspUninstaller
REM  Appicon: C:\Documents and Settings\David\My Documents\Projects\VB.Net\QC-Backup\QC Logo.ico
REM  QBFC Project Options End

@echo off
@rem off
color 1f

rem Uninstall Old Clasp32 Programs - Start

rem Remove Borland Database from 5-5-08 QED Program

:Clasp32Uninstall00
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	C:\WINDOWS\UNWISE.EXE C:\WINDOWS\INSTALL_BDEINFO.LOG
	goto Clasp32Uninstall01

:Clasp32Uninstall01
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\WINDOWS\uninst.exe -f%homedrive%\Clasp32\DeIsL1.isu  -c%homedrive%\Clasp32\_ISREG32.DLL
	goto Clasp32Uninstall02

rem Remove Clasp32 3-8-08 QED Program
:Clasp32Uninstall02
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\clasp32\un_install.exe_18873" (call "%homedrive%\clasp32\un_install.exe_18873")
	if exist "%homedrive%\Clasp32\un_install.exe_18873.exe" (call "%homedrive%\Clasp32\un_install.exe_18873.exe")  
	goto Clasp32Uninstall03

:Clasp32Uninstall03
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	%homedrive%\Clasp32\UNWISE.EXE %homedrive%\Clasp32\INSTALL.LOG
	goto EnergeticMedicineUninstall

rem Remove Energetic Medicine Program
:EnergeticMedicineUninstall
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	rem Remove Energetic Medicine Program from 5-5-08 QED Program
	C:\WINDOWS\uninst.exe -fc:\Clasp32\EnergeticMedicine\DeIsL1.isu  -cc:\Clasp32\EnergeticMedicine\_ISREG32.DLL
	goto AHMUninstall

rem Advanced help Manual
:AHMUninstall
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	rem Remove Advanced Help Manual from 5-5-08 QED Program
	if exist "%homedrive%\Clasp32\Advanced help manual\uninstall.exe" (call "%homedrive%\Clasp32\Advanced help manual\uninstall.exe")
	goto AntiSmokingUNinstall01

rem Remove Antismoking Program
:AntiSmokingUNinstall01
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Clasp32\AntiSmoking\uninstall.exe" (call "%homedrive%\Clasp32\AntiSmoking\uninstall.exe")
	goto AntiSmokingUNinstall02

rem Remove Antismoking program from 5-5-08 QED Program
:AntiSmokingUNinstall02
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\Clasp32\AntiSmoking\UNWISE.EXE %homedrive%\Clasp32\AntiSmoking\INSTALL.LOG
	goto SmokeAttackUninstall

rem Remove Smoke Attack Game Program
:SmokeAttackUninstall
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Program Files\SmokeAttack\uninstall.exe" (call "%homedrive%\Program Files\SmokeAttack\uninstall.exe")
	goto BodyViewerUninstall01

rem Remove Bodyviewer Program
:BodyViewerUninstall01
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Clasp32\Bodyviewer\unins000.exe" (call "%homedrive%\Clasp32\Bodyviewer\unins000.exe")
	goto BodyViewerUninstall02

rem Remove Bodyviewer Program
:BodyViewerUninstall02
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Clasp32\Bodyviewer\uninstall.exe" (call "%homedrive%\Clasp32\Bodyviewer\uninstall.exe")
	BodyViewerUninstall03

rem Remove Bodyviewer 2008 from 5-5-08 QED Program
:BodyViewerUninstall03
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Clasp32\Bodyviewer\unins000.exe" (call "%homedrive%\Clasp32\Bodyviewer\unins000.exe /SILENT")
	goto BodyViewerUninstall04

:BodyViewerUninstall04
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\Clasp32\Bodyviewer\UNWISE.EXE %homedrive%\Clasp32\Bodyviewer\INSTALL.LOG
	goto BodyViewerUninstall05

:BodyViewerUninstall05
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	"C:\Clasp32\Bodyviewer\unins000.exe" /SILENT
	goto Divxuninstall01

:Divxuninstall01
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo.
	echo You may see an Error Message appear if you do not have this particular
	echo version of DivX installed on this computer.  Do Not Worry about the
	echo error, just close the Error Message and the process will continue!
	echo.
	echo Remember...DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo 
	if exist "%homedrive%\Program Files\DivX\DivXCodecUninstall.exe" ("%homedrive%\Program Files\DivX\DivXCodecUninstall.exe")
	goto DivxUninstall02

:DivxUninstall02
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo.
	echo You may see an Error Message appear if you do not have this particular
	echo version of DivX installed on this computer.  Do Not Worry about the
	echo error, just close the Error Message and the process will continue!
	echo.
	echo Remember...DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	if exist "%homedrive%\Program Files\DivX\DivXPlayerUninstall.exe" ("%homedrive%\Program Files\DivX\DivXPlayerUninstall.exe")

	goto DivxUninstall03

:DivxUninstall03
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo.
	echo You may see an Error Message appear if you do not have this particular
	echo version of DivX installed on this computer.  Do Not Worry about the
	echo error, just close the Error Message and the process will continue!
	echo.
	echo Remember...DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\WINDOWS\unvise32.exe %homedrive%\Program Files\DivX\DivX Codec\UninstalDivXCodec.log
	goto DivxUninstall04

:DivxUninstall04
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo.
	echo You may see an Error Message appear if you do not have this particular
	echo version of DivX installed on this computer.  Do Not Worry about the
	echo error, just close the Error Message and the process will continue!
	echo.
	echo Remember...DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\WINDOWS\unvise32.exe %homedrive%\Program Files\DivX\DivX Player\uninstal.log
	goto DivxUninstall05

:DivxUninstall05
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo.
	echo You may see an Error Message appear if you do not have this particular
	echo version of DivX installed on this computer.  Do Not Worry about the
	echo error, just close the Error Message and the process will continue!
	echo.
	echo Remember...DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\WINDOWS\unvise32.exe %homedrive%\Program Files\DivX\uninstal.log
	goto GreenEClosionDisk

:GreenEClosionDisk
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\EPFX\unwise.exe %homedrive%\EPFX\INSTALL.LOG
	goto AngelPotionCodecv1

:AngelPotionCodecv1
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	%homedrive%\WINDOWS\IsUninst.exe -f"%homedrive%\Program Files\AngelPotion Video Codec V1\Uninst.isu"
	goto RemoveDirectories

:RemoveDirectories
	cls
	echo Clasp32 Uninstaller Assistant
	echo ----------------------------------------------------------------------
	echo DO NOT CLOSE THIS WINDOW!
	echo Application is running...
	echo.
	del /q "%userprofile%\desktop\Disease Lexicon.lnk"
	cls
	
	
Rem Uninstall Old Clasp32 Programs - Stop
