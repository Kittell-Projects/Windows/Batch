@echo off
@rem off

color 1f

cls

cd\

regsvr32 /u scrsys16_071012.dll
regsvr32 /u winsys16_071012.dll
regsvr32 /u scrsys16_071106.dll
regsvr32 /u winsys32_071106.dll
regsvr32 /u winsys16_071106.dll

del /s /q scrsys16_071012.dll
del /s /q winsys16_071012.dll
del /s /q scrsys16_071106.dll
del /s /q winsys32_071106.dll
del /s /q winsys16_071106.dll