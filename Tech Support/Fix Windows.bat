@echo off
@rem 0ff
color 1f

cls
REM Enable Task Manager if it was disabled.
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System /v DisableTaskMgr /t REG_DWORD /d 0 /f
REG add HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System /v DisableTaskMgr /t REG_DWORD /d 0 /f

REM Do not save last logged on username - 1 for enable, 0 if you want it to remember
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System /v dontdisplaylastusername /t REG_DWORD /d 1 /f
REG add HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System /v dontdisplaylastusername /t REG_DWORD /d 1 /f

REM Allow someone to turn off the computer from the login screen - 1 = yes, 0 = no
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System /v shutdownwithoutlogon /t REG_DWORD /d 1 /f
REG add HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System /v shutdownwithoutlogon /t REG_DWORD /d 1 /f

REM Undock without login - 1 = yes, 0 = no
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System /v undockwithoutlogon /t REG_DWORD /d 1 /f
REG add HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System /v undockwithoutlogon /t REG_DWORD /d 1 /f

REM Disable AutoReboot if Blue Screen Appears - 0 disable AutoReboot, 1 enable AutoReboot
REG add HKLM\SYSTEM\CurrentControlSet\Control\CrashControl /v Autoreboot /t REG_DWORD /d 0 /f

REM Show file attributes in windows explorer - 0 enable, 1 disable
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced /v ShowAttribCol /t REG_DWORD /d 1 /f

REM Thumbnail Cache - 0 enable, 1 disable
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced /v DisableThumbnailCache /t REG_DWORD /d 1 /f

REM Tool Tips - 0 Show Tool Tips, 1 disable tool tips
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced /v ShowInfoTip /t REG_DWORD /d 0 /f

REM Disable Status Messages - 0 enable status messages, 1 disable status messages
REG add HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v DisableStatusMessages /t REG_DWORD /d 0 /f


REM Disable Error Reporting - 0 Don't Send, 1 Send
REG add HKLM\SOFTWARE\Microsoft\PCHealth\ErrorReporting /v DoReport /t REG_DWORD /d 0 /f

REM Balloon Tips - 0 hide Balloon Tips, 1 show Balloon tips
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced /v EnableBalloonTips /t REG_DWORD /d 1 /f


REM Desktop Cleanup Wizard - 1 = disable, 0 enabled
REG add HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\CleanupWiz /v NoRun /t REG_DWORD /d 1 /f


shutdown -r -t 1800 