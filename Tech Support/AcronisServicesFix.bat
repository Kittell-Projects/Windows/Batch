@echo off
@rem off

color 1f

cls
echo Applying Acronis Services Fix

MsiExec.exe /X{3ED07A82-39A8-4D1A-BB36-23FCABD2AC9A}
pause

NET STOP "Acronis Scheduler2 Service"
sc config AcrSch2Svc start=disabled
sc delete AcrSch2Svc

cls
echo Acronis Services Fix has finished
pause